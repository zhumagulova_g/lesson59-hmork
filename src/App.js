import React from 'react';
import MainBlog from './containers/MainBlog/MainBlog';
import './App.css';
import JokesBlock from "./containers/JokesBlock/JokesBlock";

const App = () => {
  return (
      <div>
        <MainBlog/>
        <JokesBlock/>
      </div>
  );
};

export default App;
