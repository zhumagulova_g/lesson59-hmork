import React, {Component} from 'react';

class Movie extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps !== this.props.title;
    }

    render() {
        return (
            <div className="Movie">
               <input className="input-movie"
                      value={this.props.title}
                      onChange={(e) =>this.props.changeElem(e, this.props.id)}
               />
                <button onClick={() =>this.props.deleteElem(this.props.id)}>X</button>
            </div>
        );
    }
}

export default Movie;