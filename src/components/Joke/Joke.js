import React from 'react';

const Joke = (props) => {
    return (
        <div className="joke">
            {props.value}
        </div>
    );
}

export default Joke;