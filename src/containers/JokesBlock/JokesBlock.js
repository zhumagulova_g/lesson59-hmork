import React, {useState} from 'react';
import Joke from "../../components/Joke/Joke";
import {nanoid} from "nanoid";
import {useEffect} from 'react';

const JokesBlock = () => {
    const [jokes, setJokes] = useState([]);
    const [inputValue, setInputValue] = useState('');

    const url = 'https://api.chucknorris.io/jokes/random';

    const changeInputValue = (e) => {
        e.preventDefault();
        const value = e.target.value
        setInputValue(value);
    };

        useEffect(() => {
            let newJoke = null;

            const getRes = async() => {
                const response = await fetch(url);
                const jokes = await response.json();
                newJoke = {id: nanoid(), value: jokes.value};
                setJokes([newJoke]);
            };
            getRes().catch(e => console.error(e));
        }, []);

    return (
        <div className="Movies">
            <input className="input-jokes"
                   type="text"
                   value = {inputValue}
                    onChange={changeInputValue}
            />
            <button>Get jokes</button>
            {jokes.map(joke => (
                    <Joke
                        key={joke.id}
                        value={joke.value}
                    />
                ))}
        </div>
    );
};

export default JokesBlock;