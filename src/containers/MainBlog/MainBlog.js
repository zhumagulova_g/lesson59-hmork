import React, {Component} from 'react';
import './MainBlog.css';
import Movie from "../../components/Movie/Movie";
import {nanoid} from 'nanoid';

class MainBlog extends Component {
    state = {
        movies: [
            {title: 'Taare Zameen Par', id: nanoid()},
            {title: 'Lost girls', id: nanoid()},
            {title: 'Outlaw king', id: nanoid()},
        ],
        newMovie: '',
    }

    addMovie = (e) => {
        e.preventDefault();
        const moviesCopy = [...this.state.movies];
        moviesCopy.movies = [...moviesCopy,
            {
                title: this.state.newMovie,
                id: nanoid(),
            }
        ];
        this.setState(moviesCopy);
    }

    changeNewMovie = (e) => {
        e.preventDefault();
        const newMovieCopy = {...this.state};
        newMovieCopy.newMovie = e.target.value;
        this.setState(newMovieCopy);
    }

    changeElem = (e, id) => {
        e.preventDefault();
        const index = this.state.movies.findIndex(movie => movie.id === id);
        const newMovieCopy = [...this.state.movies];
        newMovieCopy[index].title = e.target.value;
        this.setState(newMovieCopy);
    }

    deleteHandler = (id) => {
        const index = this.state.movies.findIndex(movie => movie.id === id);
        const moviesCopy = [...this.state.movies];
        moviesCopy.splice(index, 1);
        const newState = {...this.state};
        newState.movies = moviesCopy;
        this.setState(newState);
    }

    render() {
        return (
            <div className="Movies">
                <input className="input-movies"
                       type="text"
                       value={this.state.newMovie}
                       onChange={e => this.changeNewMovie(e)}
                />
                <button className="btn-movies"
                        onClick={this.addMovie}
                        type="submit"
                >Add</button>
                {this.state.movies.map(movie => (
                    <Movie
                        id={movie.id}
                        key ={movie.id}
                        title={movie.title}
                        deleteElem={this.deleteHandler}
                        changeElem={this.changeElem}
                    />
                ))}
            </div>
        );
    }
}

export default MainBlog;